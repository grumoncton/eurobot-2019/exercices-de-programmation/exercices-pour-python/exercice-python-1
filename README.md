# Exercice de programmation python #1

Ce premier exercice de programmation en python a le but de vous familiariser
avec le programmation et le syntaxe python, de vous introduire à la
documentation et de vous encourager à penser à la motivation derrière le code.

Cet exercice est pour votre compréhension, pas pour répondre correctement aux
questions. Si vous avez des questions, n'hésitez pas à contacter un chef
d'équipe pour de l'aide.

L'exercice est très simple. Le code dans `main.py` est composé d'extraits du
code python de l'année passée. On vous demande d'ajouter des commentaires
simples décrivant son fonctionnement. Les instructions pour ce faire ce trouvent
ci-dessous.

1. Créer le dossier où vous voulez stocker cet exercice. Dans mon cas, c'était
   `C:\Users\marcel\Documents\code\grum\Eurobot2019\exercices\python\exercice_1`.
1. Ouvrez le dossier dans Visual Studio Code. Clic droit dans le dossier et
   cliquez "Open with Code".

   ![Open with code](./figures/2.open-with-code.png)

1. Cliquez le troisième onglet à gauche "Source Control", cliquez "Initialize
   Repository" puis choisissez la première option.

   ![Source Control](./figures/3.source-control.png)

1. Modifiez votre console dans VS Code à bash si ce n'est pas déjà fait. Ouvrez
   "settings" avec <kbd>ctrl + ,</kbd> puis faisez une recherche pour "shell
   path windows". Remplacez le paramètre avec `C:\Program
   Files\Git\bin\bash.exe`.

   ![Bash path](./figures/4.shell-path.png)

1. Ouvrez une console intégrée avec <kbd>ctrl+shift+p</kbd> puis "integrated
   terminal".

   ![Integrated terminal](./figures/5.integrated-terminal.png)

1. Exécutez la commande `git remote add origin ssh://gitlab@robitaille.host:49/GRUM/eurobot2019/exercices/exercice-python-1.git`.

   ![Git remote](./figures/6.git-remote.png)

1. Toujours dans l'onglet "Source Control", cliquez les trois points puis "Pull
   from...". Par la suite, cliquez "origin" puis "origin/master".

   ![Git pull](./figures/7.pull.png)

1. Maintenant, il devrait être possible de voir le fichier `main.py` dans le
   premier onglet : "Explorer".

   ![Source code](./figures/8.source-code.png)

1. Ajoutez des commentaires décrivant le fonctionnement du code. Lorsqu'une
   ligne contienne seulement `#`, ajoutez une commentaire décrivant les lignes
   en dessous. Ajoutez des commentaires à la fin décrivant l'utilité du code.

1. Revenez dans l'onglet "Source Control". Cliquez le + à coté de `main.py`,
   ajoutez un message décrivant vos changements puis cliquez le crochet.

   ![Commit](./figures/10.commit.png)

1. Ajoutez une branche git. Cliquez "master" en bas à gauche et cliquez "Create
   new branch". Inscrivez votre nom et tapez "enter".

   ![New branch](./figures/11.new-branch.png)

1. Clickez les trois points puis "Push" afin de synchroniser vos modifications
   avec le serveur.

   ![Git push](./figures/12.push.png)

1. C'est tout! Vous pouvez consulter les soumissions des autres en cliquant
   "master" dans GitLab. Envoyez moi un message et je vais vérifier votre
   soumission. Si vous faites des erreurs, je vais essayer de faire des exemples
   qui pourrons aider la compréhension.

   ![Other branches](./figures/13.other-branches.png)

