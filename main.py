import os
import time
from enum import IntEnum

# À chaque ligne commençant avec un "#", ajoutez un commentaire décrivant la/les
# ligne(s) suivante(s)


#
class TableSide(IntEnum):
    GREEN = 0
    ORANGE = 1


lcd = None
GPIO = None

#
START_PIN = 20
SIDE_PIN = 21

# Indice: `HAS_LCD` est une variable d'environnement comme `PATH`
#
if os.getenv('HAS_LCD') == 'true':
    from lcd import LCD
    lcd = LCD(0x27)

# Indice: `HAS_GPIO` est une variable d'environnement comme `PATH`
#
if os.getenv('HAS_GPIO') == 'true':
    import RPi.GPIO as GPIO

    #
    GPIO.setmode(GPIO.BCM)

    #
    GPIO.setup((START_PIN, SIDE_PIN), GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

table_side = None

#
if GPIO is not None:

    #
    while True:

        #
        time.sleep(0.1)

        #
        new_table_side = TableSide(GPIO.input(SIDE_PIN))

        #
        if table_side != new_table_side:

            #
            table_side = new_table_side

            #
            if lcd is not None:
                lcd.display_string(table_side.name.ljust(16), line=1)

        #
        if not GPIO.input(START_PIN):
            break

# En générale, ce code sert à:
#

